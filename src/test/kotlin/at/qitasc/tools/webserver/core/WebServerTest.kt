package at.qitasc.tools.webserver.core

import com.google.inject.Guice
import java.io.IOException
import java.net.ServerSocket
import java.util
import java.util.LinkedList
import java.util.Random
import java.util.concurrent.TimeUnit
import org.junit.Test as test
import kotlin.test.*

/**
 * Created by adzhyrma on 11.05.15.
 */
public class WebServerTest {
    fun isValidPort(port: Int) = port and 65535 == port

    fun isPortAvailable(port: Int) : Boolean {
        if (!isValidPort(port)) return false

        try {
            ServerSocket(port).use {
                it.setReuseAddress(true)
            }
            return true;
        } catch (exception: IOException) { }

        return false;
    }

    test fun checkWebServerStopTimeoutsAndPorts() {
        try {
            val injector = Guice.createInjector(WebServerModule())
            val webServer = injector.getInstance(javaClass<WebServer>())!!

            val random = Random()
            var numberOfPortsToListen = 3
            var ports = LinkedList<Int>()
            while(numberOfPortsToListen > 0) {
                val port = random.nextInt()

                if (!isPortAvailable(port)) continue

                try {
                    webServer.listen(port)
                    numberOfPortsToListen--
                    ports.add(port)
                } catch (exception: Exception) { }
            }

            ports.forEach { assert(!isPortAvailable(it), "Ports are not occupied by the web server") }

            val start = System.nanoTime()
            webServer.stop()
            var elapsedTime = System.nanoTime() - start

            assert(elapsedTime/1000000000.0 < 21, "Timeout exceeded 20 seconds")

            ports.forEach { assert(isPortAvailable(it), "Ports have not been freed by the web server") }
        } catch (throwable: Throwable) {
            fail(throwable.getMessage() ?: "Some error occurs")
        }
    }

    test(expected = javaClass<IllegalArgumentException>()) fun checkWebServerIllegalArguments() {
        try {
            val injector = Guice.createInjector(WebServerModule())
            val webServer = injector.getInstance(javaClass<WebServer>())!!

            setOf(-1, 65536, 1 shl 17).forEach { webServer.listen(it) }
        } catch (exception: NullPointerException) {
            fail("Some error occurs: ${exception.getMessage()}")
        }
        fail("IllegalArgumentException was not thrown")
    }

    test(expected = javaClass<IllegalStateException>()) fun checkWebServerIllegalState() {
        try {
            val injector = Guice.createInjector(WebServerModule())
            val webServer = injector.getInstance(javaClass<WebServer>())!!

            webServer.stop()
            webServer.listen(8080)
        } catch (exception: NullPointerException) {
            fail("Some error occurs: ${exception.getMessage()}")
        }
        fail("IllegalStateException was not thrown")
    }
}