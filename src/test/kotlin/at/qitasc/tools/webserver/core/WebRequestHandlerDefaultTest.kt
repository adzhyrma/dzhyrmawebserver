package at.qitasc.tools.webserver.core

import org.junit.Test as test
import kotlin.test.*
import at.qitasc.tools.webserver.core.WebRequestHandlerDefault
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.PipedInputStream
import java.io.PipedOutputStream
import java.util.LinkedList

/**
 * Created by adzhyrma on 11.05.15.
 */
public class WebRequestHandlerDefaultTest {
    fun getRequest(method: String, path: String): String {
        val request = StringBuilder{}
        request.append("$method $path HTTP/1.0\r\n")
        request.append("From: someuser@somedomain.com\r\n")
        request.append("User-Agent: HTTPTool/1.0\r\n")
        request.append("\r\n")
        return request.toString()
    }

    fun codeToHeader(code:Int) = "HTTP/1.0 ${
        when(code) {
            200 -> "200 OK"
            400 -> "400 Bad Request"
            403 -> "403 Forbidden"
            404 -> "404 Not Found"
            500 -> "500 Internal Server Error"
            501 -> "501 Not Implemented"
            else -> ""
        }
    }"

    fun expectedResponse(code: Int, json: String) = array(
            codeToHeader(code),
            "Connection: close",
            "Server: QiTascWebServer",
            "Content-Type: application/json",
            "",
            json
    )

    fun handle(webRequestHandler: WebRequestHandler, request: String) : Array<String> {
        ByteArrayInputStream(request.toByteArray("UTF-8")).use { inputStream ->
            val resultingStream = PipedInputStream()
            PipedOutputStream(resultingStream).use { outputStream ->
                webRequestHandler.handle(inputStream, outputStream)
            }

            return resultingStream.reader().useLines { it.toList().copyToArray() }
        }
    }

    fun <T> assertArrayEquals(inputArray: Array<T>, expectedArray: Array<T>, message: String) {
        assertEquals(inputArray.size(), expectedArray.size(), message)

        for(i in 0..inputArray.size()-1) {
            assertEquals(inputArray[i], expectedArray[i], message);
        }
    }

    test fun checkHandleForGetRequestWithNotExistingPath() {
        val webRequestHandler = WebRequestHandlerDefault();

        val response = expectedResponse(404, "{}")

        val actualResponse = handle(webRequestHandler, getRequest("GET", "/starwars/index.html"))
        assertArrayEquals(response, actualResponse, "Response differs from expected")
    }


    test fun checkHandleForGetRequestWithCustomRouting() {
        val webRequestHandler = WebRequestHandlerDefault();
        webRequestHandler.registerRoute("/contacts", { when(it) {
            "index.html" -> "Our contacts are xxx-xxx-xxx"
            else -> "This page doesn't exist. Use index.html"
        } })

        val response1 = expectedResponse(200, "{\"msg\":\"Our contacts are xxx-xxx-xxx\"}")
        val actualResponse1 = handle(webRequestHandler, getRequest("GET", "/contacts/index.html"))
        assertArrayEquals(response1, actualResponse1, "Response differs from expected")

        val response2 = expectedResponse(200, "{\"msg\":\"This page doesn't exist. Use index.html\"}")
        val actualResponse2 = handle(webRequestHandler, getRequest("GET", "/contacts/another.html"))
        assertArrayEquals(response2, actualResponse2, "Response differs from expected")
    }

    test fun checkHandleForGetRequestWithAboutPath() {
        val webRequestHandler = WebRequestHandlerDefault();

        val response = expectedResponse(200, "{\"msg\":\"Hello World!\"}")

        val actualResponse1 = handle(webRequestHandler, getRequest("GET", "/home/index.html"))
        assertArrayEquals(response, actualResponse1, "Response differs from expected")
    }

    test fun checkHandleForNotGetRequest() {
        val webRequestHandler = WebRequestHandlerDefault();

        val response = expectedResponse(501, "{}")
        val actualResponse = handle(webRequestHandler, getRequest("POST", "/path/index.html"))

        assertArrayEquals(response, actualResponse, "Response differs from expected")
    }
}