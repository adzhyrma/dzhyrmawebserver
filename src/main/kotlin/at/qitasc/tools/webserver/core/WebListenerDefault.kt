package at.qitasc.tools.webserver.core

import at.qitasc.tools.webserver.util.NotificationSystem
import com.google.inject.Inject
import com.google.inject.assistedinject.Assisted
import org.slf4j.LoggerFactory
import java.io.BufferedReader
import java.io.DataOutputStream
import java.io.InputStreamReader
import java.net.ServerSocket
import java.util.concurrent.Executors
import java.util.concurrent.ThreadFactory
import java.util.concurrent.TimeUnit
import kotlin.concurrent.currentThread

/**
 * Created by adzhyrma on 11.05.15.
 */
public open class WebListenerDefault [Inject](protected val webRequestHandlerFactory: WebRequestHandlerFactory, protected val notificationSystem: NotificationSystem, [Assisted] protected val port: Int) : WebListener {
    private val logger = LoggerFactory.getLogger(javaClass<WebListenerDefault>())
    private val serverSocket = ServerSocket(port)
    private val threadExecutorForWebHandlers = Executors.newCachedThreadPool(ThreadFactory {
        val newThread = Thread(it)
        newThread.setUncaughtExceptionHandler { thread, throwable ->
            logger.error("One of the web handlers for port ${serverSocket.getLocalPort()} has crashed", throwable)
        }
        newThread
    })
    var isRunning = false
        private set

    private fun isExpectedException(exception: Exception) = exception is InterruptedException || exception.getMessage() == "Socket closed"

    private fun info(message: String) {
        notificationSystem.sendMessage(message)
        logger.info(message)
    }

    override fun run() {
        info("Listening to port ${serverSocket.getLocalPort()}...")

        val webRequestHandler = webRequestHandlerFactory.create()

        isRunning = true
        try {
            while (isRunning) {
                if (currentThread.isInterrupted()) return
                try {
                    val socket = serverSocket.accept()

                    val inetAddress = socket.getInetAddress()
                    info("${inetAddress.getHostAddress()} connected to port ${serverSocket.getLocalPort()}")

                    threadExecutorForWebHandlers.execute { webRequestHandler.handle(socket.getInputStream(), socket.getOutputStream()) }
                } catch (exception: Exception) {
                    if (isExpectedException(exception)) return
                    logger.error("Socket exception on port ${serverSocket.getLocalPort()} has not been handled", exception)
                }
            }
        } finally {
            isRunning = false
        }
    }

    override fun stop() {
        if (!serverSocket.isClosed()) serverSocket.close()
        threadExecutorForWebHandlers.shutdownNow()
        if (!threadExecutorForWebHandlers.awaitTermination(2, TimeUnit.SECONDS)) {
            info("Some of the web handlers for port ${serverSocket.getLocalPort()} couldn't be finished gracefully...")
        }
    }
}