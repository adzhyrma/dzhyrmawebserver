package at.qitasc.tools.webserver.core

import javax.inject.Singleton

/**
 * Created by adzhyrma on 11.05.15.
 */
[Singleton] public class WebRequestHandlerDefaultFactory : WebRequestHandlerFactory {
    override fun create(): WebRequestHandler {
        return WebRequestHandlerDefault()
    }
}