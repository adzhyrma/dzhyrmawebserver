package at.qitasc.tools.webserver.core

import java.io.InputStream
import java.io.OutputStream

/**
 * Created by adzhyrma on 11.05.15.
 */
public trait WebRequestHandler {
    fun registerRoute(route: String, handlingFunction: (String) -> String)

    fun handle(input: InputStream, output: OutputStream)
}