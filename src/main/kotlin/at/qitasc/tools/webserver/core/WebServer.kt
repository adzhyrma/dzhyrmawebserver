package at.qitasc.tools.webserver.core

import at.qitasc.tools.webserver.util.NotificationSystem
import com.google.inject
import com.google.inject.Guice
import com.google.inject.Inject
import com.google.inject.Injector
import com.google.inject.Provider
import org.slf4j.LoggerFactory
import java.net.ServerSocket
import java.util.LinkedList
import java.util.concurrent.ThreadFactory
import java.util.concurrent.TimeUnit
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Created by adzhyrma on 11.05.15.
 */

fun main(args: Array<String>) {
    if (args.size() == 0) usage("to few arguments")

    val ports = getValidatedPorts(args)

    val injector = Guice.createInjector(WebServerModule())
    val webServer = injector.getInstance(javaClass<WebServer>())

    ports.forEach { webServer.listenWithCaughtExceptions(it) }

    var waitingForAnotherCommand = true
    while (waitingForAnotherCommand) {
        val command = readLine() ?: "exit"
        when {
            command.startsWith("listen ") -> listenToNewPort(webServer, command.substringAfter("listen ").trim())
            command == "exit" -> waitingForAnotherCommand = false
            else -> commandUsage("Command \"$command\" was not recognized")
        }
    }
    webServer.stop()
}

fun WebServer.listenWithCaughtExceptions(port:Int) {
    try {
        listen(port)
    } catch (exception: Exception) {
        println("Couldn't listen to port $port: ${exception.getMessage()}")
    }
}

fun listenToNewPort(webServer: WebServer, portAsString: String) {
    val port = getValidatedPort(portAsString, { commandUsage(it) })
    if (port != -1)
        webServer.listenWithCaughtExceptions(port)
}

fun getValidatedPorts(args: Array<String>): Set<Int> {
    val ports: IntArray = IntArray(args.size())
    for (i in 0..args.size() - 1) ports[i] = getValidatedPort(args[i], { usage(it) })
    return ports.toSet()
}

fun getValidatedPort(portAsString: String, usageHandler: (String) -> Unit): Int {
    try {
        val port = portAsString.toInt()
        if (isInvalidPort(port)) usageHandler("port number must be between 0 and 65535 inclusively")
        return port
    } catch (exception: NumberFormatException) {
        usageHandler("couldn't parse the port number: $portAsString")
    }
    return -1
}

fun isInvalidPort(port: Int) = port and 65535 != port

fun usage(msg: String) {
    println("Error:")
    println("  $msg")
    println("Usage:")
    println("  HttpServer <port> <port>...")
    println("Options:")
    println("  <port>   listen port number between 0 and 65535")
    System.exit(1)
}

fun commandUsage(msg: String) {
    println("Error:")
    println("  $msg")
    println("Usage:")
    println("  listen <port>")
    println("  exit")
    println("Options:")
    println("  <port>   listen port number between 0 and 65535")
}

class WebServer[Inject](protected val webListenerFactory: WebListenerFactory, protected val notificationSystem: NotificationSystem) {
    private val logger = LoggerFactory.getLogger(javaClass<WebServer>())
    private var httpListeners = LinkedList<WebListener>()
    private var isStopped = false
    private val threadExecutor = java.util.concurrent.Executors.newCachedThreadPool(ThreadFactory {
        val newThread = Thread(it)
        newThread.setUncaughtExceptionHandler { thread, throwable ->
            logger.error("Web listener has crashed", throwable)
        }
        newThread
    })

    init {
        Runtime.getRuntime().addShutdownHook(Thread( {
            if (isStopped)
                logger.info("Web server has been stopped normally.")
            else {
                logger.info("Web server did not stop in the specified time.")
                stop()
                logger.info("Web server was abruptly shut down.")
            }
        } ))
    }

    public synchronized fun listen(port: Int): WebListener {
        if (threadExecutor.isShutdown()) {
            logger.error("Somebody tried to listen ports after the web server was stopped")
            throw IllegalStateException("Web server has been stopped already")
        }
        if (isInvalidPort(port)) {
            logger.error("Somebody tried to listen invalid ports")
            throw IllegalArgumentException("Port numbers must be between 0 and 65535")
        }

        try {
            val listener = webListenerFactory.create(port)
            httpListeners.add(listener)
            threadExecutor.execute(listener)
            return listener
        } catch(exception: Exception) {
            logger.error("Server socket couldn't be started at port $port:", exception)
            throw exception
        }
    }

    public synchronized fun stop() {
        logger.info("Stopping the web server")
        notificationSystem.sendMessage("Shutting down all http listeners...")

        var timeOut = 20;

        threadExecutor.shutdownNow()
        httpListeners.forEach { it?.stop() }
        while (timeOut > 0) {
            if (threadExecutor.awaitTermination(1, TimeUnit.SECONDS))
                break;
            notificationSystem.sendMessage("  Waiting for $timeOut more second${if (timeOut > 1) "s" else ""}...")
            timeOut--;
        }

        if (timeOut <= 0) {
            logger.warn("Some listeners may not be closed correctly")
            notificationSystem.sendMessage("  Force shutdown")
        }

        isStopped = true
        logger.info("Web server has been stopped")
        notificationSystem.sendMessage("Done")
    }
}