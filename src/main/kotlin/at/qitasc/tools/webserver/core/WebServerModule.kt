package at.qitasc.tools.webserver.core

import at.qitasc.tools.webserver.util.NotificationSystem
import at.qitasc.tools.webserver.util.NotificationSystemConsole
import com.google.inject.AbstractModule
import com.google.inject.Provider

/**
 * Created by adzhyrma on 11.05.15.
 */
public class WebServerModule : AbstractModule() {
    override fun configure() {
        bind(javaClass<WebRequestHandlerFactory>())!!.to(javaClass<WebRequestHandlerDefaultFactory>())
        bind(javaClass<NotificationSystem>())!!.to(javaClass<NotificationSystemConsole>())
        bind(javaClass<WebListenerFactory>())!!.to(javaClass<WebListenerDefaultFactory>())
    }
}