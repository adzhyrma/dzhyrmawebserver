package at.qitasc.tools.webserver.core

import java.io.InputStream
import java.io.OutputStream
import java.util.HashMap

/**
 * Created by adzhyrma on 11.05.15.
 */
public class WebRequestHandlerDefault : WebRequestHandler {
    private val pathMapper = HashMap<String, (String) -> String>()

    init {
        pathMapper["/home"] = { index() }
        pathMapper["/about"] = { about(it) }
    }

    private fun index() = "Hello World!"

    private fun about(page: String) = "This is a simple message created by the simplest web server. Current page is ($page)"

    private fun constructResponse(returnCode: Int, jsonMessage: String): String {
        val stringBuilder = StringBuilder("HTTP/1.0 ")
        when (returnCode) {
            200 -> stringBuilder.append("200 OK")
            400 -> stringBuilder.append("400 Bad Request")
            403 -> stringBuilder.append("403 Forbidden")
            404 -> stringBuilder.append("404 Not Found")
            500 -> stringBuilder.append("500 Internal Server Error")
            501 -> stringBuilder.append("501 Not Implemented")
        }
        stringBuilder.append("\r\n")
        stringBuilder.append("Connection: close\r\n")
        stringBuilder.append("Server: QiTascWebServer\r\n")
        stringBuilder.append("Content-Type: application/json\r\n")
        stringBuilder.append("\r\n")
        stringBuilder.append(jsonMessage)

        return stringBuilder.toString()
    }

    private fun getRoute(header: String, method: String): Pair<String, String> {
        val path = header.substringBeforeLast("HTTP/").substringAfter(method).trim().toLowerCase()
        val first = path.substringBeforeLast('/')
        if (first == "")
            return Pair(path, "")
        else
            return Pair(path.substringBeforeLast('/'), path.substringAfterLast('/'))
    }

    override fun registerRoute(route: String, handlingFunction: (String) -> String) {
        pathMapper[route.toLowerCase()] = handlingFunction
    }

    override fun handle(input: InputStream, output: OutputStream) {
        input.reader().buffered().use { bufferedInput ->
            output.writer().buffered().use { bufferedOutput ->
                val header = bufferedInput.readLine()?.toUpperCase() ?: ""
                when {
                    header.startsWith("GET") -> {
                        val route = getRoute(header, "GET")
                        val message = pathMapper[route.first]?.invoke(route.second)
                        if (message == null)
                            bufferedOutput.write(constructResponse(404, "{}"))
                        else
                            bufferedOutput.write(constructResponse(200, "{\"msg\":\"$message\"}"))
                    }
                    else -> bufferedOutput.write(constructResponse(501, "{}"))
                }
            }
        }
    }
}