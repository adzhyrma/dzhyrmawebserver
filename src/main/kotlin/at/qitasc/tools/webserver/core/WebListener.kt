package at.qitasc.tools.webserver.core

/**
 * Created by andriidzhyrma on 13/05/15.
 */
public trait WebListener : Runnable{
    fun stop()
}