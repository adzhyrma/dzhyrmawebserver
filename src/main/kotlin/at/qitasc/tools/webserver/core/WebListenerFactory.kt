package at.qitasc.tools.webserver.core

import java.net.ServerSocket

/**
 * Created by andriidzhyrma on 13/05/15.
 */
public trait WebListenerFactory {
    fun create(port: Int) : WebListener
}