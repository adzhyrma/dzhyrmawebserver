package at.qitasc.tools.webserver.core

/**
 * Created by adzhyrma on 11.05.15.
 */
public trait WebRequestHandlerFactory {
    fun create(): WebRequestHandler
}