package at.qitasc.tools.webserver.core

import at.qitasc.tools.webserver.util.NotificationSystem
import com.google.inject.Inject
import java.net.ServerSocket

/**
 * Created by andriidzhyrma on 13/05/15.
 */
public class WebListenerDefaultFactory[Inject](protected val webRequestHandlerFactory: WebRequestHandlerFactory, protected val notificationSystem: NotificationSystem) : WebListenerFactory {

    override fun create(port: Int): WebListener {
        return WebListenerDefault(webRequestHandlerFactory, notificationSystem, port)
    }
}