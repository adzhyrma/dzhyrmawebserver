package at.qitasc.tools.webserver.util

/**
 * Created by andriidzhyrma on 13/05/15.
 */
public trait NotificationSystem {
    fun sendMessage(message: String)
}