package at.qitasc.tools.webserver.util

import com.google.inject.Singleton

/**
 * Created by andriidzhyrma on 13/05/15.
 */
[Singleton] public class NotificationSystemConsole : NotificationSystem {
    override synchronized fun sendMessage(message: String) {
        println(message)
    }
}